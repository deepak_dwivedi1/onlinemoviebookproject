# OnlineMovieBookProject

OnlineMovieBookProject is based on :-

1. .net Core 3.1 web api + swagger****
2. Module -> Admin /User
3. Controller-> Genere/Admin/Account/OnlineMovieBookProject
4. Used Fluent API Validation for book 5 seat and not more than 100 seat.
5. Used Clean Architecture and CQRS pattern except domain driven approaches.
6. Layers -> Presentation Layers (OnlineMovieBook.API)
            Application Layers => Business Services =Domain +Interface + Service
                                  Entity Layer= Model + Common (Validator)
                                  Core Layer = Database Layer 
7.Odata (EFCOre)

8. Test => Using Mockframework + nunit 
9. EMail + Upload Services 


             

