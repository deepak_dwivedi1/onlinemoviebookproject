﻿using FluentValidation;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Common.FluentValidation
{
    public class CartValidator : AbstractValidator<Cart>
    {
        public CartValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.MovieID).NotNull();
            RuleFor(x => x.SeatNo).NotNull();
            RuleFor(x => x.UserID).NotNull();
            RuleFor(x => x.Amount).NotNull();
            RuleFor(x => Convert.ToInt32(x.SeatNo)).LessThanOrEqualTo(100);
        }
    }
}
