﻿using FluentValidation;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Common.FluentValidation
{
    public class BookNowValidator : AbstractValidator<BookNow>
    {
        public BookNowValidator()
        {
            RuleFor(x => x.MovieDate).NotNull();
            RuleFor(x => x.MovieId).NotNull();
            RuleFor(x => x.SeatNo).NotNull();
            RuleFor(x => x.Movie_Name).NotNull();
            RuleFor(x => x.Amount).NotNull();
            RuleFor(x => Convert.ToInt32(x.SeatNo)).LessThanOrEqualTo(5);
            RuleFor(x => Convert.ToInt32(x.SeatNo)).LessThanOrEqualTo(5);
        }
    }
}
