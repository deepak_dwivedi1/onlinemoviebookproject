﻿using FluentValidation;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Common.FluentValidation
{
    public class MovieValidator : AbstractValidator<MovieDetails>
    {
        /// <summary>
        /// This validator use for Movie
        /// </summary>
        public MovieValidator()
        {
            RuleFor(x => x.MoviePicture).NotNull();
            RuleFor(x => x.Movie_Description).NotNull();
            RuleFor(x => x.Movie_Name).NotNull();
            RuleFor(x => x.MoviePicture).NotNull();
            RuleFor(x => x.DateAndTime).NotNull();
        }
    }
}
