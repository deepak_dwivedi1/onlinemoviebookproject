﻿using FluentValidation;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Common.FluentValidation
{
    /// <summary>
    /// BookTableValidator
    /// </summary>
    public class BookTableValidator : AbstractValidator<BookingTable>
    {

        public BookTableValidator()
        {
            RuleFor(x => x.MovieDetails).NotNull();
            RuleFor(x => x.MovieDetailsID).NotNull();
            RuleFor(x => x.SeatNo).NotNull();
            RuleFor(x => x.UserID).NotNull();
            RuleFor(x => x.DateToPresent).NotNull();
            RuleFor(x => x.Amount).NotNull();
            RuleFor(x => Convert.ToInt32(x.SeatNo)).LessThanOrEqualTo(5);
        }

    }
}
