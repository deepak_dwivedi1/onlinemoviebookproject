﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Country 
    {
        public Country()
        {
            this.MovieCountries = new HashSet<MovieCountry>();
        }

        [Required]
        [MaxLength(10)]
        public string Name { get; set; }

        public ICollection<MovieCountry> MovieCountries { get; set; }
    }
}
