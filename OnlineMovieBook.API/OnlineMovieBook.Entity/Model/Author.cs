﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Author
    {
        public Author()
        {
            this.Reviews = new HashSet<ReviewAuthor>();
        }

        [Required]
        [MaxLength(40)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(40)]
        public string LastName { get; set; }

        public string Email { get; set; }

        public virtual ICollection<ReviewAuthor> Reviews { get; set; }
    }
}
