﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class MovieDetails
    {
        public MovieDetails()
        {
            this.MovieGenres = new HashSet<MovieGenre>();
            this.MovieReviews = new HashSet<MovieReview>();
            this.MovieCountries = new HashSet<MovieCountry>();
            this.MovieActors = new HashSet<MovieActor>();
            //this.Projections = new HashSet<MovieProjection>();
            //this.MovieComments = new HashSet<MovieComment>();
        }
        public int Id { get; set; }
        public string Movie_Name { get; set; }
        public string Movie_Description { get; set; }
        public DateTime DateAndTime { get; set; }
        public string MoviePicture { get; set; }

        [Required]
        [MaxLength(40)]
        public string Name { get; set; }

        [Required]
        public DateTime DateOfRelease { get; set; }

        public string Resolution { get; set; } // HD, SD quality

        public decimal Rating { get; set; }

        [Required]
        [MaxLength(40)]
        public string Description { get; set; }

        [Required]
        [MaxLength(40)]
        public string Language { get; set; }

        [Required]
        public CinemaCategory CinemaCategory { get; set; } // A, B, C, D

        [MaxLength(10)]
        public string TrailerPath { get; set; }

        [Required]
        [MaxLength(30)]
        public string CoverPath { get; set; }

        [Required]
        [MaxLength(100)]
        public string WallpaperPath { get; set; }

        [MaxLength(100)]
        public string IMDBLink { get; set; }

        public int Length { get; set; }

        public int DirectorId { get; set; }

        public virtual Director Director { get; set; }

        public virtual ICollection<MovieGenre> MovieGenres { get; set; }

        public virtual ICollection<MovieReview> MovieReviews { get; set; }

        public virtual ICollection<MovieCountry> MovieCountries { get; set; }

        public virtual ICollection<MovieActor> MovieActors { get; set; }

    }
}
