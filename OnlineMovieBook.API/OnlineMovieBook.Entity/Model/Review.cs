﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Review
    {
        public Review()
        {
            this.MovieReviews = new HashSet<MovieReview>();
            this.Authors = new HashSet<ReviewAuthor>();
        }

        [Required]
        [MaxLength(10)]
        public string Title { get; set; }

        [Required]
        [MaxLength(40)]
        public string Description { get; set; }

        public DateTime Date { get; set; }

        public virtual ICollection<MovieReview> MovieReviews { get; set; }

        public virtual ICollection<ReviewAuthor> Authors { get; set; }
    }
}
