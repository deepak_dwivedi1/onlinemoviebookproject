﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Director 
    {
        public Director()
        {
            this.Movies = new HashSet<MovieDetails>();
        }

        [Required]
        [MaxLength(10)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(40)]
        public string LastName { get; set; }

        public virtual ICollection<MovieDetails> Movies { get; set; }
    }
}
