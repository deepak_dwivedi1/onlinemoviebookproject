﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Cart
    {
        public int Id { get; set; }
        public string SeatNo { get; set; }
        public string UserID { get; set; }
        public DateTime Date { get; set; }
        public int Amount { get; set; }
        public int MovieID { get; set; }
    }
}
