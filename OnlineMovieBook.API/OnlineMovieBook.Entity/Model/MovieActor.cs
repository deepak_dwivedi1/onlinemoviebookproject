﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class MovieActor
    {
        public int MovieId { get; set; }

        public virtual MovieDetails Movie { get; set; }

        public int ActorId { get; set; }

        public virtual Actor Actor { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
