﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class BookingTable
    {
        public int Id { get; set; }
        public string SeatNo { get; set; }
        public string UserID { get; set; }
        public DateTime DateToPresent { get; set; }
        public int MovieDetailsID { get; set; }
        public int Amount { get; set; }
        public virtual MovieDetails MovieDetails { get; set; }
    }
}
