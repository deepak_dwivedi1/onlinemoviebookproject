﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class MovieCountry 
    {
        public int MovieId { get; set; }

        public virtual MovieDetails Movie { get; set; }

        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
