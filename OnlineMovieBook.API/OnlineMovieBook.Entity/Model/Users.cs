﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class Users :IdentityUser
    {
        public string UserID { get; set; }
        public string Type { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordDetails { get; set; }
        public string ConcurrencyStamp { get; set; }

    }
}
