﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Entity.Model
{
    public class MovieGenre 
    {
        public int MovieId { get; set; }

        public virtual MovieDetails MovieDetails { get; set; }

        public int GenreId { get; set; }

        public virtual Genre Genre { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
