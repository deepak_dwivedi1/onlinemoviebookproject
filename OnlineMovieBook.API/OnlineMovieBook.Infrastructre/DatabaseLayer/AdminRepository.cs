﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OnlineMovieBook.BusinessServices.DomainServices;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.Infrastructre.DatabaseLayer
{
    public class AdminRepository : IAdminRepository
    {
        private ApplicationDbContext _context;
        public AdminRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Create Movie on Server
        /// </summary>
        /// <param name="movieDetails"></param>
        /// <returns></returns>
        public async Task<int> Create(MovieDetails movieDetails)
        {
            _context.MovieDetails.Add(movieDetails);
            var result=await _context.SaveChangesAsync();
            return result;
        }

        /// <summary>
        /// Check Booking Seat
        /// </summary>
        /// <returns></returns>
        public async Task<List<BookingTable>> GetBookSeat()
        {
            var getBookDetails = await _context.BookingTable.OrderByDescending(a => a.DateToPresent).ToListAsync();
            if (getBookDetails == null)
            {
                return null;
            }
            else
            return getBookDetails;
        }

        public async Task<List<Users>> GetUserDetails()
        {
            var getUsersDetails = await _context.Users.ToListAsync();
            if (getUsersDetails == null)
            {
                return null;
            }
            else
                return getUsersDetails;
        }
    }
}
