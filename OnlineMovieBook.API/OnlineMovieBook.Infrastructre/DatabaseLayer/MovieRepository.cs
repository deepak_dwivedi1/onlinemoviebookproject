﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnlineMovieBook.BusinessServices.DomainServices;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.Infrastructre.DatabaseLayer
{
    public class MovieRepository : IMovieRepository
    {
        private readonly List<MovieDetails> moviesList = new List<MovieDetails>();
        private ApplicationDbContext _context;
        bool flags = true;
        int count = 1;
        public MovieRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        //public MovieRepository()
        //{
        //    MovieDetails movieDetails = new MovieDetails()
        //    {
        //        Id = 1,
        //        DateAndTime = DateTime.Now,
        //        MoviePicture = "1",
        //        Movie_Description = "sample",
        //        Movie_Name = "wer"

        //    };
        //    moviesList.Add(movieDetails);
        //}

        /// <summary>
        /// GetAllMovieDetailsAsync
        /// </summary>
        /// <returns></returns>
        public async Task<List<MovieDetails>> GetAllMovieDetails()
        {
            var getMovieDetails = await _context.MovieDetails.ToListAsync();
            if (getMovieDetails == null)
            {
                return null;
            }
            else
                return getMovieDetails;
        }

        /// <summary>
        /// GetMovieDetailsByID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MovieDetails> GetMovieDetailsByID(int id)
        {
            var getMovieDetails = await _context.MovieDetails.Where(author => author.Id == id).FirstOrDefaultAsync(); ;
            if (getMovieDetails == null)
            {
                return null;
            }
            else
                return getMovieDetails;
        }


        /// <summary>
        /// DeleteMovieByID
        /// </summary>
        /// <param name="movieID"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMovieByID(int movieID)
        {
            var movieDetails = await GetMovieDetailsByID(movieID);
            if (movieDetails != null)
                _context.MovieDetails.Remove(movieDetails);
            var result = await _context.SaveChangesAsync();
            if (result != 0)
                return true;
            else return false;
        }

        /// <summary>
        /// Book Movie
        /// </summary>
        /// <param name="movieID">movieID</param>
        /// <returns></returns>
        public async Task<MovieDetails> GetBookMovie(int movieID)
        {
            var movieDetails = await GetMovieDetailsByID(movieID);
            //if (movieDetails != null)
            //    _context.MovieDetails.Add(movieDetails);
            //var result = await _context.SaveChangesAsync();
            //if (result != 0)
            return movieDetails;
            //else return null;
        }

        /// <summary>
        /// BookMovie
        /// </summary>
        /// <param name="bookNow"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<int> BookMovieFromSite(BookNow bookNow, string user)
        {
            List<BookingTable> bookingTables = new List<BookingTable>();
            List<Cart> carts = new List<Cart>();
            string seatNo = bookNow.SeatNo.Trim();
            int movieID = bookNow.MovieId;
            string[] seatnoArray = seatNo.Split(',');
            count = seatnoArray.Length;
            if (checkSeat(seatNo, movieID) == false)
            {
                foreach (var item in seatnoArray)
                {
                    carts.Add(new Cart { Amount = 150, MovieID = bookNow.MovieId, Date = bookNow.MovieDate, UserID = user, SeatNo = item });
                }

                foreach (var item in carts)
                {
                    _context.Cart.Add(item);
                    await _context.SaveChangesAsync();
                }
            }

            var result = await _context.SaveChangesAsync();
            if (result != 0)
                return result;
            else return result;
        }

        /// <summary>
        /// checkSeat
        /// </summary>
        /// <param name="seatNo"></param>
        /// <param name="movieID"></param>
        /// <returns></returns>
        private bool checkSeat(string seatNo, int movieID)
        {

            string seat = seatNo;
            string[] seatReserve = seat.Split(',');
            var seatNoReserveList = _context.BookingTable.Where(x => x.MovieDetailsID == movieID).ToList();
            foreach (var item in seatNoReserveList)
            {
                string alreadyBook = item.SeatNo;
                foreach (var item1 in seatReserve)
                    if (item1 == alreadyBook)
                    {
                        flags = false;
                        break;
                    }
            }
            if (flags == false)

                return true;
            else return false;

        }

        /// <summary>
        /// Check Booking Seat
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetBookSeat(DateTime movieeDate, BookNow bookNow)
        {
            string seatNo = string.Empty;

            var getBookDetails = await _context.BookingTable.Where(a => a.DateToPresent == movieeDate).ToListAsync();
            if (getBookDetails != null)
            {
                var getSeatNo = getBookDetails.Where(x => x.MovieDetailsID == bookNow.MovieId).ToList();
                if (getSeatNo != null)
                {
                    foreach (var item in getSeatNo)
                    {
                        seatNo = seatNo + " " + item.SeatNo.ToString();
                    }
                }

            }
            return seatNo;

        }

        /// <summary>
        /// GetMovieDetailsByNameAsync
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<IQueryable<MovieDetails>> GetMovieDetailsByNameAsync(string name)
        {
            var details = await _context.MovieDetails.Where(m => m.MovieGenres.Any(mg => mg.Genre.Name == name)).ToListAsync();
            var qmovieDetails= details.AsQueryable();
            return qmovieDetails;
        }
    }
}
