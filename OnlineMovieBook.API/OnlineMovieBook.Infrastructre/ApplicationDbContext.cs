﻿using Microsoft.EntityFrameworkCore;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMovieBook.Infrastructre
{
   public  class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<MovieDetails> MovieDetails { get; set; }
        public DbSet<BookingTable> BookingTable { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
