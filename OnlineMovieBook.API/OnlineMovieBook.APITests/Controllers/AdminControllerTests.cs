﻿using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OnlineMovieBook.API.Controllers;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineMovieBook.API.Controllers.Tests
{
    [TestClass()]
    public class AdminControllerTests
    {
        Mock<IAdminService> mockServices = null;
        Mock<IUploadService> uploadService = null;
        AdminController adminController = null;
        IList<IFormFile> files = null;
        MovieDetails moviesDetails = null;
        public AdminControllerTests()
        {
            moviesDetails = new MovieDetails();
            MovieDetails movieDetails = new MovieDetails()
            {
                Id = 1,
                DateAndTime = DateTime.Now,
                MoviePicture = "1",
                Movie_Description = "sample",
                Movie_Name = "StarBugs"

            };
            mockServices = new Mock<IAdminService>();
            adminController = new AdminController(mockServices.Object);
        }
        [TestMethod()]
        public async Task CreateAsyncTestAsync()
        {
            files = null;
            moviesDetails = null;
            mockServices.Setup(repo => repo.Create(files, moviesDetails)).ReturnsAsync(0);
            CancellationToken cancellationToken;
            var inte = await adminController.CreateAsync(files, cancellationToken, moviesDetails);
            Assert.IsNotNull(inte);
        }

        [TestMethod()]
        public async Task CreateAsyncPositiveTestAsync()
        {

            mockServices.Setup(repo => repo.Create(files, moviesDetails)).ReturnsAsync(0);
            CancellationToken cancellationToken;
            var inte = await adminController.CreateAsync(files, cancellationToken, moviesDetails);
            Assert.IsNotNull(inte);
        }
    }
}