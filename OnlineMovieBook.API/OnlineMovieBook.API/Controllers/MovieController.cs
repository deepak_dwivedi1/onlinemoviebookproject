﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;

namespace OnlineMovieBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        public IMovieService _movieService;
        public IUploadService _uploadService;
        private UserManager<IdentityUser> _userManager;

        public MovieController(IMovieService movieService, IUploadService uploadService, UserManager<IdentityUser> userManager)
        {
            _movieService = movieService;
            _uploadService = uploadService;
            _userManager = userManager;
        }

        /// <summary>
        /// GetAllMovie
        /// </summary>
        /// <returns></returns>
        [HttpGet("~/GetAllMovie")]
        public async Task<ActionResult<List<MovieDetails>>> GetMovieDetailsAsync()
        {
            var result = await _movieService.GetAllMovieDetailsAsync();
            if (result != null) return Ok(result);
            else return BadRequest("Movie Not Available");
        }

        /// <summary>
        /// GetMovieDetailsByID
        /// </summary>
        /// <param name="movieID">MovieID</param>
        /// <returns></returns>
        [HttpGet("~/GetAllMovieByID")]
        public async Task<ActionResult<MovieDetails>> GetMovieDetailsByIDAsync(int movieID)
        {
            if(string.IsNullOrEmpty(Convert.ToString(movieID)))
            {
                return BadRequest("id is null");
            }
            var result = await _movieService.GetMovieDetailsByIDAsync(movieID);
            if (result != null) return Ok(result);
            else return BadRequest("Movie Not Available");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movieID"></param>
        /// <returns></returns>
        [HttpDelete("~/DeleteMovieByID")]
        public async Task<ActionResult<bool>> DeleteMovieByIDAsync(int movieID)
        {
            if (string.IsNullOrEmpty(Convert.ToString(movieID)))
            {
                return BadRequest("id is null");
            }
            var result = await _movieService.DeleteMovieByID(movieID);
            if (result) return Ok(result);
            else return BadRequest("Movie Not Available");
        }

        [HttpGet(("~/BookMovie"))]
        public async Task<ActionResult<MovieDetails>> BookMovie(int movieID)
        {
            if (string.IsNullOrEmpty(Convert.ToString(movieID)))
            {
                return BadRequest("id is null");
            }
            var result = await _movieService.BookMovie(movieID);
            if (result != null) return Ok(result);
            else return BadRequest("Movie Not Available");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movieDetails"></param>
        /// <returns></returns>
        [HttpPost("~/BookNow")]
        public async Task<ActionResult<int>> BookMovieFromSite(BookNow bookNow)
        {
            var user = _userManager.GetUserAsync(HttpContext.User).ToString();
            if (bookNow == null)
            {
                return BadRequest("movieDetails is null");
            }
            var result = await _movieService.BookMovieFromSite(bookNow, user);
            if (result!=0) return Ok(result);
            else return BadRequest("Movie Not Available");
        }

        /// <summary>
        /// CheckBookSeat
        /// </summary>
        /// <returns></returns>
        [HttpPost("~/CheckSeat")]
        public async Task<ActionResult<string>> CheckBookSeatAsync(DateTime movieeDate,BookNow bookNow)
        {
            var result = await _movieService.CheckBookSeatAsync(movieeDate, bookNow);
            return result;
        }
    }
}