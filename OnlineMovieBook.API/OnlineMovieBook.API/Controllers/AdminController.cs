﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;

namespace OnlineMovieBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private IAdminService _adminService;
        /// <summary>
        /// adminService
        /// </summary>
        /// <param name="adminService"></param>
        public AdminController(IAdminService adminService)
        {
            this._adminService = adminService;
        }

        /// <summary>
        /// Create Movie in DB
        /// </summary>
        /// <param name="files">Pictures</param>
        /// <param name="movieDetails">MovieDetails</param>
        /// <returns></returns>
        [HttpPost("~/Create")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<int>> CreateAsync(IList<IFormFile> files, CancellationToken cancellationToken, [FromForm] MovieDetails movieDetails)
        {
            return await _adminService.Create(files, movieDetails);
        }

        /// <summary>
        /// CheckBookSeat
        /// </summary>
        /// <returns></returns>
        [HttpGet("~/CheckSeat")]
        public async Task<ActionResult<List<BookingTable>>> CheckBookSeatAsync()
        {
            var result = await _adminService.CheckBookSeatAsync();
            return result;
        }

        /// <summary>
        /// GetUserNameAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet("~/GetUserName")]
        public async Task<ActionResult<List<Users>>> GetUserNameAsync()
        {
            var result = await _adminService.GetUserDetails();
            return result;
        }

    }
}