﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity;
using OnlineMovieBook.Entity.Model;

namespace OnlineMovieBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IEmailService _sender;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ILogger logger;
        private readonly RoleManager<IdentityRole> roleManager;
        public AccountController(UserManager<IdentityUser> userManager, IEmailService _sender, SignInManager<IdentityUser> signInManager, ILogger logger, RoleManager<IdentityRole> roleManager)
        {
            this._userManager = userManager;
            this._sender = _sender;
            this.signInManager = signInManager;
            this.logger = logger;
            this.roleManager = roleManager;
        }


        //public async Task<ActionResult> Login(string returnURl=null)
        //{
        //    //Clear the external cookies
        //    await HttpContext.signoutAsnc.
        //}

        [HttpPost("~/Register")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([FromForm] Register model)
        {

            var user = new IdentityUser { UserName = model.Email, Email = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                logger.LogInformation("User created a new account with password.");

                await signInManager.SignInAsync(user, isPersistent: false);
                logger.LogInformation("User created a new account with password.");
                return Ok();
            }

            // If we got this far, something failed, redisplay form
            return BadRequest();
        }

        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("~/Login")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(Login model, string returnUrl = null)
        {

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, set lockoutOnFailure: true
            var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                logger.LogInformation("User logged in.");
                return null;
            }
            if (result.IsLockedOut)
            {
                logger.LogWarning("User account locked out.");
                return null;
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpPost("~/Logout")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            logger.LogInformation("User logged out.");
            return null;
        }
    }
}