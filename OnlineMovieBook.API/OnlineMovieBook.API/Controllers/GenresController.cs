﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;

namespace OnlineMovieBook.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenresController : ControllerBase
    {
        private const int GenresPerPage = 12;

        private readonly IMovieService moviesService;

        public GenresController(IMovieService moviesService)
        {
            this.moviesService = moviesService;
        }

        [HttpGet("~/MoviesByGenere")]
        public async Task<ActionResult<PaginatedList<MovieDetails>>> ByName(int? pageNumber, string name)
        {
            var moviesByGenreName = await moviesService.GetByGenreNameAsQueryable(name);

            if (moviesByGenreName.Count() == 0)
            {
                return this.NotFound();
            }

          
            var moviesByGenreNamePaginated = await PaginatedList<MovieDetails>
                    .CreateAsync(moviesByGenreName, pageNumber ?? 1, GenresPerPage);

            return moviesByGenreNamePaginated;
        }
    }
}