using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.Extensions.Logging;
using OnlineMovieBook.BusinessServices.DomainServices;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.BusinessServices.ServiceLayer;
using OnlineMovieBook.Entity.Model;
using OnlineMovieBook.Infrastructre;
using OnlineMovieBook.Infrastructre.DatabaseLayer;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace OnlineMovieBook.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        [Obsolete]
        public IHostingEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen();


            // Add EF services to the services container.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //Add Distributed Cache
            //services.AddDistributedSqlServerCache(options =>
            //{
            //    options.ConnectionString = this.configuration.GetConnectionString("DefaultConnection");
            //    options.SchemaName = "dbo";
            //    options.TableName = "CacheData";
            //});


            services.AddSession(options =>
            {
                options.IdleTimeout = new TimeSpan(0, 6, 0, 0);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            //Add Identity ROle
            services.AddIdentity<IdentityUser, IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>();

            //DI Injection
            services.AddScoped<UserManager<IdentityUser>>();
            // requires
            // using Microsoft.AspNetCore.Identity.UI.Services;
            // using WebPWrecover.Services;
            services.AddTransient<IEmailService, EmailService>();
            services.Configure<AuthMessageSenderOptions>(Configuration);
            services.AddTransient<IMovieService, MovieService>();
            services.AddTransient<IUploadService, UploadService>();
            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<IAdminRepository, AdminRepository>();
            services.AddTransient<IMovieRepository, MovieRepository>();

            //Fluent Validator
            services.AddControllers()
                                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            //Fluent Validation
            services.AddMvc(setup =>
            {
                //...mvc setup...
            }).AddFluentValidation();
            //Add odata
            //services.AddOData();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [Obsolete]
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostingEnvironment env1)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseAuthentication();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
