﻿using OnlineMovieBook.BusinessServices.DomainServices;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.ServiceLayer
{
    public class MovieService : IMovieService
    {
        public IMovieRepository _movieRepository;
        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        /// <summary>
        /// GetAllMovieDetails
        /// </summary>
        /// <returns></returns>
        public async Task<List<MovieDetails>> GetAllMovieDetailsAsync()
        {
            return await _movieRepository.GetAllMovieDetails();
        }

        /// <summary>
        /// GetMovieDetailsByID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MovieDetails> GetMovieDetailsByIDAsync(int id)
        {
            return await _movieRepository.GetMovieDetailsByID(id);
        }

        /// <summary>
        /// DeleteMovieByID
        /// </summary>
        /// <param name="movieID"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMovieByID(int movieID)
        {
            return await _movieRepository.DeleteMovieByID(movieID);
        }

        /// <summary>
        /// BookMovie
        /// </summary>
        /// <param name="movieID">movieID</param>
        /// <returns></returns>
        public async Task<MovieDetails> BookMovie(int movieID)
        {
            return await _movieRepository.GetBookMovie(movieID);
        }

        /// <summary>
        /// BookMovieFromSite
        /// </summary>
        /// <param name="bookNow"></param>
        /// <returns></returns>
        public async Task<int> BookMovieFromSite(BookNow bookNow, string user)
        {
            return await _movieRepository.BookMovieFromSite(bookNow,user);
        }

        /// <summary>
        /// CheckBookSeatAsync
        /// </summary>
        /// <returns></returns>
        public async Task<string> CheckBookSeatAsync(DateTime movieeDate, BookNow bookNow)
        {
            var result = await _movieRepository.GetBookSeat(movieeDate, bookNow);
            return result;
        }

        /// <summary>
        /// GetByGenreNameAsQueryable
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<IQueryable<MovieDetails>> GetByGenreNameAsQueryable(string name)
        {
            var movies = await _movieRepository.GetMovieDetailsByNameAsync(name);
               
            return movies;
        }


    }
}
