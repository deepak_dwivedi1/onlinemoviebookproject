﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.ServiceLayer
{
    public class UploadService : IUploadService
    {
        private IHostingEnvironment hostingEnvironment;
        public UploadService(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }
        public async Task<FileStream> uploadMultipleFile(IList<IFormFile> files)
        {
            long totalBytes = files.Sum(f => f.Length);
            FileStream output = null;
            foreach (IFormFile item in files)
            {
                string fileName = item.FileName.Trim('"');
                byte[] buffer = new byte[16 * 1024];
                using (output = System.IO.File.Create(this.GetpathAndFileName(fileName)))
                {
                    using (Stream input = item.OpenReadStream())
                    {
                        long totalReadBytes = 0;
                        int readBytes;
                        while ((readBytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            await output.WriteAsync(buffer, 0, readBytes);
                            totalBytes += readBytes;
                        }
                    }

                }

            }
            return output;

        }

        private string GetpathAndFileName(string fileName)
        {
            string path = this.hostingEnvironment.WebRootPath + "\\upload";
            if (!Directory.Exists(path))

                Directory.CreateDirectory(path);
            return path + fileName;

        }

        /// <summary>
        /// CheckExistFIle
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool CheckIfExcelFile(IFormFile file)
        {
            var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
            return (extension == ".xlsx" || extension == ".xls"); // Change the extension based on your need
        }
        /// <summary>
        /// WriteFile
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private async Task<bool> WriteFile(IFormFile file)
        {
            bool isSaveSuccess = false;
            string fileName;
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = DateTime.Now.Ticks + extension; //Create a new Name for the file due to security reasons.

                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files");

                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }

                var path = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files",
                   fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                isSaveSuccess = true;
            }
            catch (Exception e)
            {
                //log error
            }

            return isSaveSuccess;
        }

        /// <summary>
        /// UploadFile
        /// </summary>
        /// <param name="file">file</param>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns></returns>
        public async Task<bool> UploadFile(IFormFile file, CancellationToken cancellationToken)
        {
            bool result = false;
            if (CheckIfExcelFile(file))
            {
                result = await WriteFile(file);
            }
            else
            {
                return false;
            }

            return result;
        }

    }
}
