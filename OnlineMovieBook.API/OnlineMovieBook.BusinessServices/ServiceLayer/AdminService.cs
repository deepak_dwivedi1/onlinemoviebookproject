﻿using Microsoft.AspNetCore.Http;
using OnlineMovieBook.BusinessServices.DomainServices;
using OnlineMovieBook.BusinessServices.InterfaceLayer;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.ServiceLayer
{
    public class AdminService : IAdminService
    {
        private IAdminRepository _adminRepository;
        private IUploadService uploadService;
        public AdminService(IAdminRepository adminRepository, IUploadService uploadService)
        {
            this._adminRepository = adminRepository;
            this.uploadService = uploadService;
        }

      

        /// <summary>
        /// Create & Upload Movie In Blob + DB
        /// </summary>
        /// <param name="files"></param>
        /// <param name="movieDetails"></param>
        /// <returns></returns>
        public async Task<int> Create(IList<IFormFile> files, MovieDetails movieDetails)
        {
            //upload 
            foreach (var item in files)
            {
                movieDetails.MoviePicture = "~/uploads/" + item.FileName.Trim();
            }
            //upload to blob
            var uploadServices = await uploadService.uploadMultipleFile(files);
            if (uploadServices == null) return 0;
            var result = await _adminRepository.Create(movieDetails);
            return result;
        }

        /// <summary>
        /// CheckBookSeatAsync
        /// </summary>
        /// <returns></returns>
        public async Task<List<BookingTable>> CheckBookSeatAsync()
        {
            var result = await _adminRepository.GetBookSeat();
            return result;
        }

        /// <summary>
        /// GetUserDetails
        /// </summary>
        /// <returns></returns>
        public async Task<List<Users>> GetUserDetails()
        {
            var result = await _adminRepository.GetUserDetails();
            return result;
        }
    }
}
