﻿using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.InterfaceLayer
{
    public interface IMovieService
    {
    
        Task<bool> DeleteMovieByID(int movieID);
        Task<MovieDetails> GetMovieDetailsByIDAsync(int movieID);
        Task<List<MovieDetails>> GetAllMovieDetailsAsync();
        Task<MovieDetails> BookMovie(int movieID);
        Task<int> BookMovieFromSite(BookNow bookNow, string user);
        Task<string> CheckBookSeatAsync(DateTime movieeDate,BookNow bookNow);
        Task<IQueryable<MovieDetails>> GetByGenreNameAsQueryable(string name);
    }
}
