﻿using Microsoft.AspNetCore.Http;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.InterfaceLayer
{
    public interface IAdminService
    {
        Task<int> Create(IList<IFormFile> files, MovieDetails movieDetails);
        Task<List<BookingTable>> CheckBookSeatAsync();
        Task<List<Users>> GetUserDetails();
    }
}
