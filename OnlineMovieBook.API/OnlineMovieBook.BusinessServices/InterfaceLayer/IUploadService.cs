﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.InterfaceLayer
{
    public interface IUploadService
    {
        Task<FileStream> uploadMultipleFile(IList<IFormFile> files);

        Task<bool> UploadFile(
          IFormFile file,
          CancellationToken cancellationToken);
    }
}
