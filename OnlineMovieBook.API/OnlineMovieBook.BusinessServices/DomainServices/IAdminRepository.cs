﻿using Microsoft.AspNetCore.Http;
using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.DomainServices
{
    public interface IAdminRepository
    {
        Task<int> Create(MovieDetails movieDetails);
        Task<List<BookingTable>> GetBookSeat();
        Task<List<Users>> GetUserDetails();
    }
}
