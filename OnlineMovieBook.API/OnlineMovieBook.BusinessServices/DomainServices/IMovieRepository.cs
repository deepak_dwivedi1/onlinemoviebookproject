﻿using OnlineMovieBook.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMovieBook.BusinessServices.DomainServices
{
    public interface IMovieRepository
    {
        Task<List<MovieDetails>> GetAllMovieDetails();
        Task<MovieDetails> GetMovieDetailsByID(int id);
        Task<bool> DeleteMovieByID(int movieID);
        Task<MovieDetails> GetBookMovie(int movieID);
        Task<int> BookMovieFromSite(BookNow bookNow, string user);
        Task<string> GetBookSeat(DateTime movieeDate, BookNow bookNow);
        Task<IQueryable<MovieDetails>> GetMovieDetailsByNameAsync(string name);
    }
}
